/*
 * netfilter.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NETFILTER_CLASS_HPP
#define NETFILTER_CLASS_HPP

#include <string>
#include <sstream>
#include <vector>
#include "airvpntools.hpp"
#include "localnetwork.hpp"

#if defined(__linux__) && !defined(__ANDROID__)

#include <iostream>
#include "loadmod.h"

#endif

extern void global_log(const std::string &s);

class NetFilter
{
    public:

    enum class Mode
    {
        OFF,
        AUTO,
        NFTABLES,
        IPTABLES,
        PF,
        UNKNOWN
    };

    enum class Protocol
    {
        UDP,
        TCP,
        ANY
    };

    enum class IP
    {
        v4,
        v6,
        ANY
    };

    enum class Direction
    {
        INPUT,
        OUTPUT
    };

    NetFilter(const std::string &workdir, Mode mode = Mode::AUTO);
    ~NetFilter();

    bool systemBackupExists();
    bool init();
    bool restore();
    void setup(const std::string &loopbackIface);
    bool commitRules();
    bool addAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool addRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRemoveAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRemoveRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    Mode getMode();
    bool setMode(Mode mode);
    std::string getModeDescription();
    std::string getModeDescription(Mode mode);
    bool addIgnoredInterface(const std::string &interface);
    void clearIgnoredInterfaces();
    bool isFirewalldRunning();
    bool isUfwRunning();
    bool isIPTablesLegacy();
    bool isNetworkLockAvailable();
    bool isNetworkLockEnabled();

    bool isIptableFilterAvailable();
    bool isIptableNatAvailable();
    bool isIptableMangleAvailable();
    bool isIptableSecurityAvailable();
    bool isIptableRawAvailable();

    bool isIp6tableFilterAvailable();
    bool isIp6tableNatAvailable();
    bool isIp6tableMangleAvailable();
    bool isIp6tableSecurityAvailable();
    bool isIp6tableRawAvailable();

    private:

    const char *systemctlBinary = "systemctl";
    const char *shellBinary = "sh";

    bool checkService(const std::string &name);
    bool readFile(const std::string &fname, char *buffer, int size);

    // nftables

    const char *nftBinary = "nft";

    bool nftablesSave();
    bool nftablesRestore();
    bool nftablesFlush();
    void nftablesSetup(const std::string &loopbackIface);
    void nftablesResetRules();
    std::string createNftablesGenericRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void nftablesAddRule(const std::string &rule);
    bool nftablesCommitRule(const std::string &rule);
    bool nftablesCommit();

    std::string nftablesSaveFile = "nftables-save.txt";
    std::string nftablesNetFilterRulesFile = "nftables-netfilter-rules.txt";
    std::string nftablesRules = "";

    // iptables

    const char *iptablesBinary = NULL;
    const char *iptablesSaveBinary = NULL;
    const char *iptablesRestoreBinary = NULL;
    const char *iptablesCurrentBinary = "iptables";
    const char *iptablesCurrentSaveBinary = "iptables-save";
    const char *iptablesCurrentRestoreBinary = "iptables-restore";
    const char *iptablesLegacyBinary = "iptables-legacy";
    const char *iptablesLegacySaveBinary = "iptables-legacy-save";
    const char *iptablesLegacyRestoreBinary = "iptables-legacy-restore";
    const char *ip6tablesBinary = NULL;
    const char *ip6tablesSaveBinary = NULL;
    const char *ip6tablesRestoreBinary = NULL;
    const char *ip6tablesCurrentBinary = "ip6tables";
    const char *ip6tablesCurrentSaveBinary = "ip6tables-save";
    const char *ip6tablesCurrentRestoreBinary = "ip6tables-restore";
    const char *ip6tablesLegacyBinary = "ip6tables-legacy";
    const char *ip6tablesLegacySaveBinary = "ip6tables-legacy-save";
    const char *ip6tablesLegacyRestoreBinary = "ip6tables-legacy-restore";

    bool iptableFilterAvailable;
    bool iptableNatAvailable;
    bool iptableMangleAvailable;
    bool iptableSecurityAvailable;
    bool iptableRawAvailable;

    bool ip6tableFilterAvailable;
    bool ip6tableNatAvailable;
    bool ip6tableMangleAvailable;
    bool ip6tableSecurityAvailable;
    bool ip6tableRawAvailable;

    bool iptablesSave(IP ip);
    bool iptablesRestore(IP ip);
    bool iptablesFlush(IP ip);
    void iptablesSetup(const std::string &loopbackIface);
    void iptablesResetRules(IP ip);
    std::string createIptablesGenericRule(Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void iptablesAddRule(IP ip, const std::string &rule);
    bool iptablesCommitRule(IP ip, const std::string &rule);
    bool iptablesCommit(IP ip);

    std::string iptablesSaveFile = "iptables-save.txt";
    std::string ip6tablesSaveFile = "ip6tables-save.txt";
    std::string iptablesRules = "";
    std::string ip6tablesRules = "";

    // pf

    const char *pfctlBinary = "pfctl";

    bool pfEnable();
    bool pfSave();
    bool pfRestore();
    bool pfFlush();
    bool pfFlushAll();
    bool pfFlushRules();
    void pfSetup(const std::string &loopbackIface);
    void pfResetRules();
    std::string createPfGenericRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void pfAddRule(const std::string &rule);
    bool pfCommit();
    bool pfAddIgnoredInterface(const std::string &interface);

    std::string pfSaveFile = "pf-save.txt";
    std::string pfNetFilterRulesFile = "pf-netfilter-rules.txt";
    std::string pfConfFile = "/etc/pf.conf";
    std::string pfRules = "";

    AirVPNTools::InitSystemType initSystemType = AirVPNTools::InitSystemType::Unknown;

    char binpath[128];

    char *charBuffer = NULL;

    const int charBufferSize = 65536;

    Mode filterMode = Mode::UNKNOWN;
    std::string workingDirectory = "";
    std::string loopbackInterface = "";
    std::vector<std::string> ignoredInterface;

    LocalNetwork *localNetwork = nullptr;

    bool firewalldAvailable = false;
    bool ufwAvailable = false;
    bool nftablesAvailable = false;
    bool iptablesAvailable = false;
    bool iptablesLegacy = false;
    bool pfAvailable = false;
    bool pfNeedsFullFlush = true;
    bool networkLockEnabled = false;

#if defined(__linux__) && !defined(__ANDROID__)

    bool loadLinuxModule(const std::string &module_name, const std::string &module_params);
    bool loadLinuxModule(const char *module_name, const char *module_params);

#endif

};

class NetFilterException : public std::exception
{
    public:

    explicit NetFilterException(const char* errorMessage)
    {
        exceptionMessage = errorMessage;
    }

    explicit NetFilterException(const std::string &errorMessage)
    {
        exceptionMessage = errorMessage;
    }

    virtual ~NetFilterException() throw()
    {
    }

    virtual const char *what() const throw()
    {
       return exceptionMessage.c_str();
    }

    protected:

    std::string exceptionMessage;
};

#endif
