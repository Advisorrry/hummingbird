/*
 * netfilter.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <sstream>
#include <cstring>
#include <iterator>
#include <vector>
#include <algorithm>
#include <regex>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include "include/netfilter.hpp"
#include "include/execproc.h"

NetFilter::NetFilter(const std::string &workdir, Mode mode)
{
    if(workdir == "")
        throw(NetFilterException("No working directory provided for object NetFilter"));

    if(mode == Mode::UNKNOWN)
        throw(NetFilterException("Unknown mode cannot be used for NetFilter construction"));

    workingDirectory = workdir;

    firewalldAvailable = false;
    ufwAvailable = false;
    nftablesAvailable = false;
    iptablesAvailable = false;
    iptablesLegacy = false;
    pfAvailable = false;
    pfNeedsFullFlush = true;
    networkLockEnabled = false;

    initSystemType = AirVPNTools::getInitSystemType();

    loopbackInterface = "";

    clearIgnoredInterfaces();

    charBuffer = (char *)malloc(charBufferSize);

    if(charBuffer == NULL)
        throw(NetFilterException("Cannot allocate memory for NetFilter buffer"));

    firewalldAvailable = checkService("firewalld");

    ufwAvailable = checkService("ufw");

    // nftables

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
        nftablesAvailable = true;
    else
        nftablesAvailable = false;

    // iptables

    get_exec_path(iptablesLegacyBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        iptablesBinary = iptablesLegacyBinary;
        iptablesSaveBinary = iptablesLegacySaveBinary;
        iptablesRestoreBinary = iptablesLegacyRestoreBinary;

        ip6tablesBinary = ip6tablesLegacyBinary;
        ip6tablesSaveBinary = ip6tablesLegacySaveBinary;
        ip6tablesRestoreBinary = ip6tablesLegacyRestoreBinary;

        iptablesAvailable = true;
        iptablesLegacy = true;
    }
    else
    {
        get_exec_path(iptablesCurrentBinary, binpath);

        if(strcmp(binpath, "") != 0)
        {
            iptablesBinary = iptablesCurrentBinary;
            iptablesSaveBinary = iptablesCurrentSaveBinary;
            iptablesRestoreBinary = iptablesCurrentRestoreBinary;

            ip6tablesBinary = ip6tablesCurrentBinary;
            ip6tablesSaveBinary = ip6tablesCurrentSaveBinary;
            ip6tablesRestoreBinary = ip6tablesCurrentRestoreBinary;

            iptablesAvailable = true;
            iptablesLegacy = true;
        }
        else
        {
            iptablesAvailable = false;
            iptablesLegacy = false;
        }
    }

    iptableFilterAvailable = false;
    iptableNatAvailable = false;
    iptableMangleAvailable = false;
    iptableSecurityAvailable = false;
    iptableRawAvailable = false;

    ip6tableFilterAvailable = false;
    ip6tableNatAvailable = false;
    ip6tableMangleAvailable = false;
    ip6tableSecurityAvailable = false;
    ip6tableRawAvailable = false;

    // pf

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
        pfAvailable = true;
    else
        pfAvailable = false;

    localNetwork = new LocalNetwork();

    if(localNetwork == nullptr)
        throw(NetFilterException("Cannot create a LocalNetwork object for NetFilter"));

    setMode(mode);
}

NetFilter::~NetFilter()
{
    if(filterMode != Mode::OFF)
        restore();

    if(charBuffer != NULL)
        free(charBuffer);
    
    if(localNetwork != nullptr)
        delete localNetwork;
}

bool NetFilter::systemBackupExists()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    fileName = workingDirectory + "/" + nftablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + iptablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + ip6tablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    fileName = workingDirectory + "/" + pfSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval = true;

    return retval;
}

bool NetFilter::init()
{
    bool retval = false;
    std::ostringstream os;

    if(filterMode == Mode::OFF)
    {
        global_log("Network filter and lock are disabled");

        networkLockEnabled = false;

        return true;
    }

    if(filterMode == Mode::AUTO || filterMode == Mode::UNKNOWN)
    {
        networkLockEnabled = false;

        return false;
    }

    os.str("");

    os << "Network filter and lock are using " << getModeDescription();

    global_log(os.str());

#if defined(__linux__) && !defined(__ANDROID__)

    if(filterMode == Mode::NFTABLES)
    {
        loadLinuxModule("nf_tables", "");
    }
    else if(filterMode == Mode::IPTABLES)
    {
        iptableFilterAvailable = loadLinuxModule("iptable_filter", "");
        iptableNatAvailable = loadLinuxModule("iptable_nat", "");
        iptableMangleAvailable = loadLinuxModule("iptable_mangle", "");
        iptableSecurityAvailable = loadLinuxModule("iptable_security", "");
        iptableRawAvailable = loadLinuxModule("iptable_raw", "");

        ip6tableFilterAvailable = loadLinuxModule("ip6table_filter", "");
        ip6tableNatAvailable = loadLinuxModule("ip6table_nat", "");
        ip6tableMangleAvailable = loadLinuxModule("ip6table_mangle", "");
        ip6tableSecurityAvailable = loadLinuxModule("ip6table_security", "");
        ip6tableRawAvailable = loadLinuxModule("ip6table_raw", "");

        // This is needed to force the initial loading of rules at boot in distributions running under kernel 5.15.x and iptables 1,8.7

        commitAllowRule(NetFilter::IP::v4, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "169.254.68.69", 0);
        commitRemoveAllowRule(NetFilter::IP::v4, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "169.254.68.69", 0);

        commitAllowRule(NetFilter::IP::v6, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "fc00::1", 0);
        commitRemoveAllowRule(NetFilter::IP::v6, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, "fc00::1", 0);
    }

    if(isFirewalldRunning())
    {
        os.str("");

        os << "WARNING: firewalld is running on this system and may interfere with network filter and lock";

        global_log(os.str());
    }

    if(isUfwRunning())
    {
        os.str("");

        os << "WARNING: ufw is running on this system and may interfere with network filter and lock";

        global_log(os.str());
    }

#endif

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = nftablesSave();
        }
        break;

        case Mode::IPTABLES:
        {
            retval = iptablesSave(IP::v4);

            if(retval == true)
                retval = iptablesSave(IP::v6);
            else
                retval = false;
        }
        break;

        case Mode::PF:
        {
            pfEnable();

            retval = pfSave();
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = false;

    return retval;
}

bool NetFilter::restore()
{
    bool retval = true;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    fileName = workingDirectory + "/" + nftablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= nftablesRestore();

    fileName = workingDirectory + "/" + iptablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= iptablesRestore(IP::v4);

    fileName = workingDirectory + "/" + ip6tablesSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= iptablesRestore(IP::v6);

    fileName = workingDirectory + "/" + pfSaveFile;

    if(access(fileName.c_str(), F_OK) == 0)
        retval &= pfRestore();

    networkLockEnabled = false;

    return retval;
}

void NetFilter::setup(const std::string &loopbackIface)
{
    loopbackInterface = loopbackIface;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            nftablesSetup(loopbackIface);
        }
        break;

        case Mode::IPTABLES:
        {
            iptablesSetup(loopbackIface);
        }
        break;

        case Mode::PF:
        {
            pfSetup(loopbackIface);
        }
        break;

        default:
        {
        }
        break;
    }
}

bool NetFilter::addAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesAddRule("add " + rule + " accept");

                retval = true;
            }
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-A ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j ACCEPT";

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "pass ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            pfAddRule(rule);

            retval = true;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    return retval;
}

bool NetFilter::addRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesAddRule("add " + rule + " drop");

                retval = true;
            }
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-A ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j DROP";

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "block ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            pfAddRule(rule);

            retval = true;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    return retval;
}

bool NetFilter::commitRules()
{
    bool retval = false;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = nftablesCommit();
        }
        break;

        case Mode::IPTABLES:
        {
            retval = iptablesCommit(IP::v4);

            if(retval == true)
                retval = iptablesCommit(IP::v6);
        }
        break;

        case Mode::PF:
        {
            retval = pfCommit();
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = true;

    return retval;
}

bool NetFilter::commitAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = true;

            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesCommitRule("insert " + rule + " accept");

                retval &= true;
            }
            else
                retval &= false;

            // save rule

            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesAddRule("add " + rule + " accept");

                retval &= true;
            }
            else
                retval &= false;
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-I ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j ACCEPT";

            iptablesCommitRule(ip, rule);

            // Save rule

            rule = "-A ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j ACCEPT";

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "pass ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            pfAddRule(rule);

            pfCommit();

            retval = true;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = true;

    return retval;
}

bool NetFilter::commitRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            retval = true;

            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesCommitRule("insert " + rule + " drop");
                
                retval &= true;
            }
            else
                retval &= false;

            // Save rule

            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                nftablesAddRule("add " + rule + " drop");

                retval &= true;
            }
            else
                retval &= false;
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-I ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j DROP";

            iptablesCommitRule(ip, rule);

            // Save rule

            rule = "-A ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j DROP";

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "block ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            pfAddRule(rule);

            pfCommit();

            retval = true;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    return retval;
}

bool NetFilter::commitRemoveAllowRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                rule = "add " + rule + " accept\n";

                size_t pos = nftablesRules.find(rule);

                if(pos != std::string::npos)
                {
                    // Remove this rule from saved rules

                    nftablesRules.replace(pos, rule.length(), "");

                    retval = nftablesCommit();
                }
                else
                    retval = false;
            }
            else
                retval = false;
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-D ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j ACCEPT";

            iptablesCommitRule(ip, rule);

            // Save rule

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "pass ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);
            rule += "\n";

            size_t pos = pfRules.find(rule);

            if(pos != std::string::npos)
            {
                // Remove this rule from saved rules

                pfRules.replace(pos, rule.length(), "");

                retval = pfCommit();
            }
            else
                retval = false;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = true;

    return retval;
}

bool NetFilter::commitRemoveRejectRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    bool retval = false;
    std::string rule;

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
            rule = createNftablesGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            if(rule != "")
            {
                rule += "add " + rule + " drop\n";

                size_t pos = nftablesRules.find(rule);

                if(pos != std::string::npos)
                {
                    // Remove this rule from saved rules

                    nftablesRules.replace(pos, rule.length(), "");

                    retval = nftablesCommit();
                }
                else
                    retval = false;
            }
            else
                retval = false;
        }
        break;

        case Mode::IPTABLES:
        {
            rule = "-D ";

            rule += createIptablesGenericRule(direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);

            rule += " -j DROP";

            iptablesCommitRule(ip, rule);

            // Save rule

            iptablesAddRule(ip, rule);

            retval = true;
        }
        break;

        case Mode::PF:
        {
            rule = "block ";

            rule += createPfGenericRule(ip, direction, interface, protocol, sourceIP, sourcePort, destinationIP, destinationPort);
            rule += "\n";

            size_t pos = pfRules.find(rule);

            if(pos != std::string::npos)
            {
                // Remove this rule from saved rules

                pfRules.replace(pos, rule.length(), "");

                retval = pfCommit();
            }
            else
                retval = false;
        }
        break;

        default:
        {
            retval = false;
        }
        break;
    }

    networkLockEnabled = true;

    return retval;
}

bool NetFilter::setMode(Mode mode)
{
    bool retval = false;
    std::string errorMessage;

    switch(mode)
    {
        case Mode::AUTO:
        {
            if(nftablesAvailable == true)
            {
                filterMode = Mode::NFTABLES;

                retval = true;
            }
            else if(iptablesAvailable == true)
            {
                filterMode = Mode::IPTABLES;

                retval = true;
            }
            else if(pfAvailable == true)
            {
                filterMode = Mode::PF;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("No usable firewall found in this system"));
            }
        }
        break;

        case Mode::NFTABLES:
        {
            if(nftablesAvailable == true)
            {
                filterMode = Mode::NFTABLES;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("nftables is not available in this system"));
            }
        }
        break;

        case Mode::IPTABLES:
        {
            if(iptablesAvailable == true)
            {
                filterMode = Mode::IPTABLES;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("iptables or iptables legacy is not available in this system"));
            }
        }
        break;

        case Mode::PF:
        {
            if(pfAvailable == true)
            {
                filterMode = Mode::PF;

                retval = true;
            }
            else
            {
                filterMode = Mode::OFF;

                throw(NetFilterException("pf is not available in this system"));
            }
        }
        break;

        case Mode::OFF:
        {
            filterMode = Mode::OFF;

            retval = true;
        }
        break;

        default:
        {
            filterMode = Mode::OFF;

            throw(NetFilterException("Unknown firewall system"));
        }
        break;
    }

    return retval;
}

NetFilter::Mode NetFilter::getMode()
{
    return filterMode;
}

std::string NetFilter::getModeDescription(NetFilter::Mode mode)
{
    std::string description;

    switch(mode)
    {
        case Mode::AUTO:
        {
            if(nftablesAvailable == true)
                description = "nftables";
            else if(iptablesAvailable == true)
                description = iptablesBinary;
            else if(pfAvailable == true)
                description = "pf";
            else
                description = "unknown";
        }
        break;

        case Mode::NFTABLES:
        {
            description = "nftables";
        }
        break;

        case Mode::IPTABLES:
        {
            description = iptablesBinary;
        }
        break;

        case Mode::PF:
        {
            description = "pf";
        }
        break;

        default:
        {
            description = "unknown";
        }
        break;
    }

    return description;
}

std::string NetFilter::getModeDescription()
{
    return getModeDescription(filterMode);
}

bool NetFilter::addIgnoredInterface(const std::string &interface)
{
    if(std::find(ignoredInterface.begin(), ignoredInterface.end(), interface) != ignoredInterface.end())
        return false;

    ignoredInterface.push_back(interface);

    switch(filterMode)
    {
        case Mode::NFTABLES:
        {
        }
        break;

        case Mode::IPTABLES:
        {
        }
        break;

        case Mode::PF:
        {
            pfAddIgnoredInterface(interface);
        }
        break;

        default:
        {
        }
        break;
    }

    return true;
}

void NetFilter::clearIgnoredInterfaces()
{
    ignoredInterface.clear();
}

bool NetFilter::checkService(const std::string &name)
{
    bool retval = false;

    if(initSystemType == AirVPNTools::InitSystemType::Systemd)
    {
        get_exec_path(systemctlBinary, binpath);

        if(strcmp(binpath, "") != 0)
        {
            if(execute_process(NULL, NULL, binpath, "is-active", "--quiet", name.c_str(), NULL) == 0)
                retval = true;
            else
                retval = false;
        }
        else
            retval = false;
    }
    else if(initSystemType == AirVPNTools::InitSystemType::SystemVinit)
    {
        char init_script[32], buf[128];

        strcpy(init_script, "/etc/init.d/");
        strcat(init_script, name.c_str());

        get_exec_path(shellBinary, binpath);

        if(execute_process(NULL, buf, binpath, init_script, "status", NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::isFirewalldRunning()
{
    return firewalldAvailable;
}

bool NetFilter::isUfwRunning()
{
    return ufwAvailable;
}

bool NetFilter::isIPTablesLegacy()
{
    return iptablesLegacy;
}

bool NetFilter::isNetworkLockAvailable()
{
    bool networkLockAvailable = true;

    if(filterMode == NetFilter::Mode::OFF || filterMode == NetFilter::Mode::UNKNOWN)
        networkLockAvailable = false;

    return networkLockAvailable;
}

bool NetFilter::isNetworkLockEnabled()
{
    return networkLockEnabled;
}

bool NetFilter::readFile(const std::string &fname, char *buffer, int size)
{
    bool retval = false;
    FILE *fp;
    struct stat fnamestat;
    int fsize;
    size_t nc;

    if(buffer == NULL || size <= 0)
        return false;

    if(access(fname.c_str(), F_OK) == 0)
    {
        stat(fname.c_str(), &fnamestat);

        fsize = fnamestat.st_size;

        if(fsize < size)
        {
            fp = fopen(fname.c_str(), "r");

            nc = fread(buffer, 1, fsize, fp);

            fclose(fp);

            if(nc > 0)
            {
                buffer[fsize] = '\0';

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

// nftables

void NetFilter::nftablesResetRules()
{
    nftablesRules = "";
}

std::string NetFilter::createNftablesGenericRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string ipTag, ifTag;
    std::string rule = "rule ";

    ipTag = "ip";

    if(ip == IP::v6)
    {
        if(localNetwork->isIPv6Enabled() == true)
            ipTag += "6";
        else
            return "";
    }

    rule += ipTag + " filter ";

    if(direction == Direction::INPUT)
    {
        rule += "INPUT";
        ifTag = "iifname";
    }
    else
    {
        rule += "OUTPUT";
        ifTag = "oifname";
    }

    if(interface != "")
        rule += " " + ifTag + " " + interface;

    if(sourceIP != "")
        rule += " " + ipTag + " saddr " + sourceIP;

    if(destinationIP != "")
        rule += " " + ipTag + " daddr " + destinationIP;

    if(protocol != Protocol::ANY)
    {
        rule += " ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourcePort > 0)
    {
        rule += " sport ";
        rule += sourcePort;
    }

    if(destinationPort > 0)
    {
        rule += " dport ";
        rule += destinationPort;
    }

    rule += " counter";

    return rule;
}

void NetFilter::nftablesAddRule(const std::string &rule)
{
    nftablesRules += rule;
    nftablesRules += "\n";
}

bool NetFilter::nftablesCommitRule(const std::string &rule)
{
    bool retval = false;
    char *exec_args[EXEC_MAX_ARGS];
    int n;

    strcpy(binpath, "");

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        retval = true;

        std::istringstream buf(rule);
        std::istream_iterator<std::string> beg(buf), end;

        std::vector<std::string> tokens(beg, end);

        n = 0;

        exec_args[n++] = binpath;

        for(auto& s: tokens)
        {
            exec_args[n++] = (char *)s.c_str();

            if(n == EXEC_MAX_ARGS)
                return false;
        }

        exec_args[n++] = NULL;

        if(execute_process_args(NULL, NULL, binpath, exec_args) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesCommit()
{
    bool retval = false;
    std::string ruleFileName, rules = nftablesRules;
    std::ofstream outputFile;

    ruleFileName = workingDirectory;
    ruleFileName += "/";
    ruleFileName += nftablesNetFilterRulesFile;

    if(access(ruleFileName.c_str(), F_OK) == 0)
        unlink(ruleFileName.c_str());

    strcpy(binpath, "");

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        nftablesFlush();

        rules += "add rule ip filter OUTPUT counter drop\n";
        
        if(localNetwork->isIPv6Enabled() == true)
            rules += "add rule ip6 filter OUTPUT counter drop\n";

        outputFile.open(ruleFileName);

        if(outputFile.good())
        {
            outputFile << rules << std::endl;

            outputFile.close();
            
            if(execute_process((char *)rules.c_str(), NULL, binpath, "-f", ruleFileName.c_str(), NULL) == 0)
                retval = true;
            else
                retval = false;

            unlink(ruleFileName.c_str());
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesSave()
{
    bool retval = false;
    std::ofstream dumpFile;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += nftablesSaveFile;

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, charBuffer, binpath, "list", "ruleset", NULL) == 0)
        {
            dumpFile.open(fileName);

            if(dumpFile.good())
            {
                dumpFile << charBuffer << std::endl;

                dumpFile.close();

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesRestore()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += nftablesSaveFile;

    get_exec_path(nftBinary, binpath);

    if(strcmp(binpath, "") != 0 && access(fileName.c_str(), F_OK) == 0)
    {
        nftablesFlush();

        if(execute_process(NULL, NULL, binpath, "-f", fileName.c_str(), NULL) == 0)
        {
            unlink(fileName.c_str());

            retval = true;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::nftablesFlush()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(nftBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        retval = true;

        if(execute_process(NULL, NULL, cmdpath, "flush", "ruleset", NULL) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::nftablesSetup(const std::string &loopbackIface)
{
    nftablesResetRules();

    // IPv4

    nftablesAddRule("add table ip mangle");
    nftablesAddRule("add chain ip mangle PREROUTING { type filter hook prerouting priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle INPUT { type filter hook input priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle FORWARD { type filter hook forward priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle OUTPUT { type route hook output priority -150; policy accept; }");
    nftablesAddRule("add chain ip mangle POSTROUTING { type filter hook postrouting priority -150; policy accept; }");
    nftablesAddRule("add table ip nat");
    nftablesAddRule("add chain ip nat PREROUTING { type nat hook prerouting priority -100; policy accept; }");
    nftablesAddRule("add chain ip nat INPUT { type nat hook input priority 100; policy accept; }");
    nftablesAddRule("add chain ip nat OUTPUT { type nat hook output priority -100; policy accept; }");
    nftablesAddRule("add chain ip nat POSTROUTING { type nat hook postrouting priority 100; policy accept; }");
    nftablesAddRule("add table ip filter");
    nftablesAddRule("add chain ip filter INPUT { type filter hook input priority 0; policy drop; }");
    nftablesAddRule("add chain ip filter FORWARD { type filter hook forward priority 0; policy drop; }");
    nftablesAddRule("add chain ip filter OUTPUT { type filter hook output priority 0; policy drop; }");

    // Local input

    nftablesAddRule("add rule ip filter INPUT iifname \"lo\" counter accept");

    // Accept DHCP

    nftablesAddRule("add rule ip filter INPUT ip saddr 255.255.255.255 counter accept");

    // Accept local network

    nftablesAddRule("add rule ip filter INPUT ip saddr 192.168.0.0/16 ip daddr 192.168.0.0/16 counter accept");
    nftablesAddRule("add rule ip filter INPUT ip saddr 10.0.0.0/8 ip daddr 10.0.0.0/8 counter accept");
    nftablesAddRule("add rule ip filter INPUT ip saddr 172.16.0.0/12 ip daddr 172.16.0.0/12 counter accept");

    // Accept ping

    // nftablesAddRule("add rule ip filter INPUT icmp type echo-request counter accept");

    // Accept established sessions

    nftablesAddRule("add rule ip filter INPUT ct state related,established  counter accept");

    // Accept all tun interfaces

    nftablesAddRule("add rule ip filter INPUT iifname \"tun*\" counter accept");

    // Reject everything else

    nftablesAddRule("add rule ip filter INPUT counter drop");

    // Accept TUN forward

    nftablesAddRule("add rule ip filter FORWARD iifname \"tun*\" counter accept");

    // Reject all the other forwarding

    nftablesAddRule("add rule ip filter FORWARD counter drop");

    // Local output

    nftablesAddRule("add rule ip filter OUTPUT oifname \"lo\" counter accept");

    // Accept DHCP

    nftablesAddRule("add rule ip filter OUTPUT ip daddr 255.255.255.255 counter accept");

    // Accept local network

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 192.168.0.0/16 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 10.0.0.0/8 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 172.16.0.0/12 counter accept");

    // Allow multicast

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 224.0.0.0/24 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 224.0.0.0/24 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 224.0.0.0/24 counter accept");

    // Simple Service Discovery Protocol address

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 239.255.255.250 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 239.255.255.250 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 239.255.255.250 counter accept");

    // Service Location Protocol version 2 address

    nftablesAddRule("add rule ip filter OUTPUT ip saddr 192.168.0.0/16 ip daddr 239.255.255.253 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 10.0.0.0/8 ip daddr 239.255.255.253 counter accept");
    nftablesAddRule("add rule ip filter OUTPUT ip saddr 172.16.0.0/12 ip daddr 239.255.255.253 counter accept");

    // Allow ping

    // nftablesAddRule("add rule ip filter OUTPUT icmp type echo-reply counter accept");

    // Allow all TUN interfaces

    nftablesAddRule("add rule ip filter OUTPUT oifname \"tun*\" counter accept");

    // Allow established sessions

    nftablesAddRule("add rule ip filter OUTPUT ct state established  counter accept");

    // IPv6

    if(localNetwork->isIPv6Enabled() == true)
    {
        nftablesAddRule("add table ip6 mangle");
        nftablesAddRule("add chain ip6 mangle PREROUTING { type filter hook prerouting priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle INPUT { type filter hook input priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle FORWARD { type filter hook forward priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle OUTPUT { type route hook output priority -150; policy accept; }");
        nftablesAddRule("add chain ip6 mangle POSTROUTING { type filter hook postrouting priority -150; policy accept; }");
        nftablesAddRule("add table ip6 nat");
        nftablesAddRule("add chain ip6 nat PREROUTING { type nat hook prerouting priority -100; policy accept; }");
        nftablesAddRule("add chain ip6 nat INPUT { type nat hook input priority 100; policy accept; }");
        nftablesAddRule("add chain ip6 nat OUTPUT { type nat hook output priority -100; policy accept; }");
        nftablesAddRule("add chain ip6 nat POSTROUTING { type nat hook postrouting priority 100; policy accept; }");
        nftablesAddRule("add table ip6 filter");
        nftablesAddRule("add chain ip6 filter INPUT { type filter hook input priority 0; policy drop; }");
        nftablesAddRule("add chain ip6 filter FORWARD { type filter hook forward priority 0; policy drop; }");
        nftablesAddRule("add chain ip6 filter OUTPUT { type filter hook output priority 0; policy drop; }");

        // Accept local network

        nftablesAddRule("add rule ip6 filter INPUT iifname \"lo\" counter accept");

        // Reject traffic to localhost not coming from local interface

        nftablesAddRule("add rule ip6 filter INPUT iifname != \"lo\" ip6 saddr ::1 counter reject");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter INPUT rt type 0 counter drop");
            
        // nftablesAddRule("add rule ip6 filter INPUT icmpv6 type echo-request counter accept");

        // Allow private network

        nftablesAddRule("add rule ip6 filter INPUT ip6 saddr fe80::/10 counter accept");

        // Allow multicast

        nftablesAddRule("add rule ip6 filter INPUT ip6 daddr ff00::/8 counter accept");

        // Allow ping

        // nftablesAddRule("add rule ip6 filter INPUT meta l4proto ipv6-icmp counter accept");

        // Allow established sessions

        nftablesAddRule("add rule ip6 filter INPUT ct state related,established  counter accept");

        // Allow all TUN interfaces

        nftablesAddRule("add rule ip6 filter INPUT iifname \"tun*\" counter accept");

        // Reject everything else

        nftablesAddRule("add rule ip6 filter INPUT counter drop");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter FORWARD rt type 0 counter drop");

        // Allow TUN forwarding

        nftablesAddRule("add rule ip6 filter FORWARD iifname \"tun*\" counter accept");

        // Reject every other forwarding

        nftablesAddRule("add rule ip6 filter FORWARD counter drop");

        // Allow local traffic

        nftablesAddRule("add rule ip6 filter OUTPUT oifname \"lo\" counter accept");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        nftablesAddRule("add rule ip6 filter OUTPUT rt type 0 counter drop");

        // Allow private network

        nftablesAddRule("add rule ip6 filter OUTPUT ip6 saddr fe80::/10 counter accept");

        // Allow multicast

        nftablesAddRule("add rule ip6 filter OUTPUT ip6 daddr ff00::/8 counter accept");

        // Allow ping

        nftablesAddRule("add rule ip6 filter OUTPUT meta l4proto ipv6-icmp counter accept");

        // Allow TUN

        nftablesAddRule("add rule ip6 filter OUTPUT oifname \"tun*\" counter accept");

        // Allow established sessions

        nftablesAddRule("add rule ip6 filter OUTPUT ct state established  counter accept");
    }
}

// iptables

bool NetFilter::isIptableFilterAvailable()
{
    return iptableFilterAvailable;
}

bool NetFilter::isIptableNatAvailable()
{
    return iptableNatAvailable;
}

bool NetFilter::isIptableMangleAvailable()
{
    return iptableMangleAvailable;
}

bool NetFilter::isIptableSecurityAvailable()
{
    return iptableSecurityAvailable;
}

bool NetFilter::isIptableRawAvailable()
{
    return iptableRawAvailable;
}

bool NetFilter::isIp6tableFilterAvailable()
{
    return ip6tableFilterAvailable;
}

bool NetFilter::isIp6tableNatAvailable()
{
    return ip6tableNatAvailable;
}

bool NetFilter::isIp6tableMangleAvailable()
{
    return ip6tableMangleAvailable;
}

bool NetFilter::isIp6tableSecurityAvailable()
{
    return ip6tableSecurityAvailable;
}

bool NetFilter::isIp6tableRawAvailable()
{
    return ip6tableRawAvailable;
}

void NetFilter::iptablesResetRules(IP ip)
{
    if(ip == IP::v4)
        iptablesRules = "";
    else
        ip6tablesRules = "";
}

std::string NetFilter::createIptablesGenericRule(Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string rule = "";

    if(direction == Direction::INPUT)
        rule += "INPUT";
    else
        rule += "OUTPUT";

    if(interface != "")
    {
        if(direction == Direction::INPUT)
            rule += " -i ";
        else
            rule += " -o ";

        rule += interface;
    }

    if(protocol != Protocol::ANY)
    {
        rule += " -p ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourceIP != "")
    {
        rule += " -s " + sourceIP;
    }

    if(sourcePort > 0)
    {
        rule += " --sport ";
        rule += sourcePort;
    }

    if(destinationIP != "")
    {
        rule += " -d " + destinationIP;
    }

    if(destinationPort > 0)
    {
        rule += " --dport ";
        rule += destinationPort;
    }

    return rule;
}

void NetFilter::iptablesAddRule(IP ip, const std::string &rule)
{
    if(ip == IP::v4)
    {
        iptablesRules += rule;
        iptablesRules += "\n";
    }
    else
    {
        ip6tablesRules += rule;
        ip6tablesRules += "\n";
    }
}

bool NetFilter::iptablesCommitRule(IP ip, const std::string &rule)
{
    bool retval = false;
    char *exec_args[EXEC_MAX_ARGS];
    int n;

    strcpy(binpath, "");

    if(ip == IP::v4)
        get_exec_path(iptablesBinary, binpath);
    else
        get_exec_path(ip6tablesBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        retval = true;

        std::istringstream buf(rule);
        std::istream_iterator<std::string> beg(buf), end;

        std::vector<std::string> tokens(beg, end);

        n = 0;

        exec_args[n++] = binpath;

        for(auto& s: tokens)
        {
            exec_args[n++] = (char *)s.c_str();

            if(n == EXEC_MAX_ARGS)
                return false;
        }

        exec_args[n++] = NULL;

        if(execute_process_args(NULL, NULL, binpath, exec_args) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesCommit(IP ip)
{
    bool retval = false;
    std::string rules;

    strcpy(binpath, "");

    if(ip == IP::v4)
    {
        get_exec_path(iptablesRestoreBinary, binpath);

        rules = iptablesRules;
    }
    else
    {
        get_exec_path(ip6tablesRestoreBinary, binpath);

        rules = ip6tablesRules;
    }

    if(strcmp(binpath, "") != 0)
    {
        iptablesFlush(ip);

        rules += "-A OUTPUT -j DROP\n";
        rules += "COMMIT\n";

        if(execute_process((char *)rules.c_str(), NULL, binpath, NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesSave(IP ip)
{
    bool retval = false;
    std::ofstream dumpFile;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";

    if(ip == IP::v4)
    {
        get_exec_path(iptablesSaveBinary, binpath);

        fileName += iptablesSaveFile;
    }
    else
    {
        get_exec_path(ip6tablesSaveBinary, binpath);

        fileName += ip6tablesSaveFile;
    }

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, charBuffer, binpath, NULL) == 0)
        {
            dumpFile.open(fileName);

            if(dumpFile.good())
            {
                dumpFile << charBuffer << std::endl;

                dumpFile.close();

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesRestore(IP ip)
{
    bool retval = false;
    std::string backupFileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    backupFileName = workingDirectory;
    backupFileName += "/";

    if(ip == IP::v4)
    {
        get_exec_path(iptablesRestoreBinary, binpath);

        backupFileName += iptablesSaveFile;
    }
    else
    {
        get_exec_path(ip6tablesRestoreBinary, binpath);

        backupFileName += ip6tablesSaveFile;
    }

    if(strcmp(binpath, "") != 0 && access(backupFileName.c_str(), F_OK) == 0)
    {
        if(readFile(backupFileName.c_str(), charBuffer, charBufferSize) == true)
        {
            iptablesFlush(ip);

            if(execute_process(charBuffer, NULL, binpath, NULL) == 0)
            {
                unlink(backupFileName.c_str());

                retval = true;
            }
            else
                retval = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::iptablesFlush(IP ip)
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    if(ip == IP::v4)
        get_exec_path(iptablesBinary, cmdpath);
    else
        get_exec_path(ip6tablesBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        retval = true;

        if(iptableFilterAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "filter", "-F", NULL) != 0)
                retval = false;
        }
   
        if(iptableNatAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "nat", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableMangleAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "mangle", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableRawAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "raw", "-F", NULL) != 0)
                retval = false;
        }

        if(iptableSecurityAvailable == true)
        {
            if(execute_process(NULL, NULL, cmdpath, "-t", "security", "-F", NULL) != 0)
                retval = false;
        }

        if(execute_process(NULL, NULL, cmdpath, "-F", NULL) != 0)
            retval = false;

        if(execute_process(NULL, NULL, cmdpath, "-X", NULL) != 0)
            retval = false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::iptablesSetup(const std::string &loopbackIface)
{
    // IPv4

    iptablesResetRules(IP::v4);

    if(iptableMangleAvailable == true)
    {
        iptablesAddRule(IP::v4, "*mangle");
        iptablesAddRule(IP::v4, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":FORWARD ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v4, "COMMIT");
    }

    if(iptableNatAvailable == true)
    {
        iptablesAddRule(IP::v4, "*nat");
        iptablesAddRule(IP::v4, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v4, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v4, "COMMIT");
    }

    if(iptableFilterAvailable == true)
    {
        iptablesAddRule(IP::v4, "*filter");
        iptablesAddRule(IP::v4, ":INPUT DROP [0:0]");
        iptablesAddRule(IP::v4, ":FORWARD DROP [0:0]");
        iptablesAddRule(IP::v4, ":OUTPUT DROP [0:0]");
    }

    // Local input

    iptablesAddRule(IP::v4, "-A INPUT -i " + loopbackIface + " -j ACCEPT");

    // Accept DHCP

    iptablesAddRule(IP::v4, "-A INPUT -s 255.255.255.255/32 -j ACCEPT");

    // Accept local network

    iptablesAddRule(IP::v4, "-A INPUT -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A INPUT -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A INPUT -s 172.16.0.0/12 -d 172.16.0.0/12 -j ACCEPT");

    // Accept ping

    // iptablesAddRule(IP::v4, "-A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT");

    // Accept established sessions

    iptablesAddRule(IP::v4, "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT");

    // Accept all tun interfaces

    iptablesAddRule(IP::v4, "-A INPUT -i tun+ -j ACCEPT");

    // Reject everything else

    iptablesAddRule(IP::v4, "-A INPUT -j DROP");

    // Accept TUN forward

    iptablesAddRule(IP::v4, "-A FORWARD -i tun+ -j ACCEPT");

    // Reject all the other forwarding

    iptablesAddRule(IP::v4, "-A FORWARD -j DROP");

    // Local output

    iptablesAddRule(IP::v4, "-A OUTPUT -o " + loopbackIface + " -j ACCEPT");

    // Accept DHCP

    iptablesAddRule(IP::v4, "-A OUTPUT -d 255.255.255.255/32 -j ACCEPT");

    // Accept local network

    iptablesAddRule(IP::v4, "-A OUTPUT -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 172.16.0.0/12 -d 172.16.0.0/12 -j ACCEPT");

    // Allow multicast

    iptablesAddRule(IP::v4, "-A OUTPUT -s 192.168.0.0/16 -d 224.0.0.0/24 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 10.0.0.0/8 -d 224.0.0.0/24 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 172.16.0.0/12 -d 224.0.0.0/24 -j ACCEPT");

    // Simple Service Discovery Protocol address

    iptablesAddRule(IP::v4, "-A OUTPUT -s 192.168.0.0/16 -d 239.255.255.250/32 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 10.0.0.0/8 -d 239.255.255.250/32 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 172.16.0.0/12 -d 239.255.255.250/32 -j ACCEPT");

    // Service Location Protocol version 2 address

    iptablesAddRule(IP::v4, "-A OUTPUT -s 192.168.0.0/16 -d 239.255.255.253/32 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 10.0.0.0/8 -d 239.255.255.253/32 -j ACCEPT");
    iptablesAddRule(IP::v4, "-A OUTPUT -s 172.16.0.0/12 -d 239.255.255.253/32 -j ACCEPT");

    // Allow ping

    iptablesAddRule(IP::v4, "-A OUTPUT -p icmp -m icmp --icmp-type 0 -j ACCEPT");

    // Allow all TUN interfaces

    iptablesAddRule(IP::v4, "-A OUTPUT -o tun+ -j ACCEPT");

    // Allow established sessions

    iptablesAddRule(IP::v4, "-A OUTPUT -m state --state ESTABLISHED -j ACCEPT");

    // IPv6

    iptablesResetRules(IP::v6);

    if(ip6tableMangleAvailable == true)
    {
        iptablesAddRule(IP::v6, "*mangle");
        iptablesAddRule(IP::v6, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":FORWARD ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v6, "COMMIT");
    }

    if(ip6tableNatAvailable == true)
    {
        iptablesAddRule(IP::v6, "*nat");
        iptablesAddRule(IP::v6, ":PREROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":INPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":OUTPUT ACCEPT [0:0]");
        iptablesAddRule(IP::v6, ":POSTROUTING ACCEPT [0:0]");
        iptablesAddRule(IP::v6, "COMMIT");
    }

    if(ip6tableFilterAvailable == true)
    {
        iptablesAddRule(IP::v6, "*filter");
        iptablesAddRule(IP::v6, ":INPUT DROP [0:0]");
        iptablesAddRule(IP::v6, ":FORWARD DROP [0:0]");
        iptablesAddRule(IP::v6, ":OUTPUT DROP [0:0]");

        // Accept local network

        iptablesAddRule(IP::v6, "-A INPUT -i " + loopbackIface + " -j ACCEPT");

        // Reject traffic to localhost not coming from local interface

        iptablesAddRule(IP::v6, "-A INPUT -s ::1/128 ! -i " + loopbackIface + " -j REJECT --reject-with icmp6-port-unreachable");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IP::v6, "-A INPUT -m rt --rt-type 0 -j DROP");

        // icmpv6-type:router-advertisement - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IP::v6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 134 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:neighbor-solicitation - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IP::v6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 135 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:neighbor-advertisement - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IP::v6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 136 -m hl --hl-eq 255 -j ACCEPT");

        // icmpv6-type:redirect - Rules which are required for your IPv6 address to be properly allocated

        // iptablesAddRule(IP::v6, "-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 137 -m hl --hl-eq 255 -j ACCEPT");

        // Allow private network

        iptablesAddRule(IP::v6, "-A INPUT -s fe80::/10 -j ACCEPT");

        // Allow multicast

        iptablesAddRule(IP::v6, "-A INPUT -d ff00::/8 -j ACCEPT");

        // Allow ping

        // iptablesAddRule(IP::v6, "-A INPUT -p ipv6-icmp -j ACCEPT");

        // Allow established sessions

        iptablesAddRule(IP::v6, "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT");

        // Allow all TUN interfaces

        iptablesAddRule(IP::v6, "-A INPUT -i tun+ -j ACCEPT");

        // Reject everything else

        iptablesAddRule(IP::v6, "-A INPUT -j DROP");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IP::v6, "-A FORWARD -m rt --rt-type 0 -j DROP");

        // Allow TUN forwarding

        iptablesAddRule(IP::v6, "-A FORWARD -i tun+ -j ACCEPT");

        // Reject every other forwarding

        iptablesAddRule(IP::v6, "-A FORWARD -j DROP");

        // Allow local traffic

        iptablesAddRule(IP::v6, "-A OUTPUT -o " + loopbackIface + " -j ACCEPT");

        // Disable processing of any RH0 packet which could allow a ping-pong of packets

        iptablesAddRule(IP::v6, "-A OUTPUT -m rt --rt-type 0 -j DROP");

        // Allow private network

        iptablesAddRule(IP::v6, "-A OUTPUT -s fe80::/10 -j ACCEPT");

        // Allow multicast

        iptablesAddRule(IP::v6, "-A OUTPUT -d ff00::/8 -j ACCEPT");

        // Allow ping

        // iptablesAddRule(IP::v6, "-A OUTPUT -p ipv6-icmp -j ACCEPT");

        // Allow TUN

        iptablesAddRule(IP::v6, "-A OUTPUT -o tun+ -j ACCEPT");

        // Allow established sessions

        iptablesAddRule(IP::v6, "-A OUTPUT -m state --state ESTABLISHED -j ACCEPT");
    }
}

// pf

void NetFilter::pfResetRules()
{
    pfRules = "";
}

std::string NetFilter::createPfGenericRule(IP ip, Direction direction, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort)
{
    std::string ifTag;
    std::string rule = "";

    if(direction == Direction::INPUT)
        rule += "in";
    else
        rule += "out";

    rule += " quick";

    if(interface != "")
        rule += " on " + interface;

    rule += " inet";

    if(ip == IP::v6)
    {
        if(localNetwork->isIPv6Enabled() == true)
            rule += "6";
        else
            return "";
    }

    if(protocol != Protocol::ANY)
    {
        rule += " proto ";

        if(protocol == Protocol::TCP)
            rule += "tcp";
        else
            rule += "udp";
    }

    if(sourceIP != "")
    {
        rule += " from " + sourceIP;

        if(sourcePort > 0)
        {
            rule += " port ";
            rule += sourcePort;
        }
    }

    if(destinationIP != "")
    {
        rule += " to " + destinationIP;

        if(destinationPort > 0)
        {
            rule += " port ";
            rule += destinationPort;
        }
    }

    rule += " keep state";

    return rule;
}

void NetFilter::pfAddRule(const std::string &rule)
{
    pfRules += rule;
    pfRules += "\n";
}

bool NetFilter::pfEnable()
{
    bool retval = false;
    std::string ruleFileName, rules;
    std::ofstream outputFile;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        if(execute_process(NULL, NULL, binpath, "-e", NULL) == 0)
            retval = true;
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfCommit()
{
    bool retval = false;
    std::string ruleFileName, rules;
    std::ofstream outputFile;

    if(workingDirectory == "")
        return false;

    rules = pfRules;

    rules += "block all\n";

    ruleFileName = workingDirectory;
    ruleFileName += "/";
    ruleFileName += pfNetFilterRulesFile;

    if(access(ruleFileName.c_str(), F_OK) == 0)
        unlink(ruleFileName.c_str());

    strcpy(binpath, "");

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0)
    {
        pfFlush();

        outputFile.open(ruleFileName);

        if(outputFile.good())
        {
            outputFile << rules << std::endl;

            outputFile.close();

            if(execute_process(NULL, NULL, binpath, "-f", ruleFileName.c_str(), NULL) == 0)
                retval = true;
            else
                retval = false;

            unlink(ruleFileName.c_str());
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfSave()
{
    std::ifstream inputFile;
    std::ofstream outputFile;
    std::string outputFileName;

    if(workingDirectory == "")
        return false;

    if(access(pfConfFile.c_str(), F_OK) != 0)
        return false;

    outputFileName = workingDirectory;
    outputFileName += "/";
    outputFileName += pfSaveFile;

    inputFile.open(pfConfFile);
    outputFile.open(outputFileName);

    outputFile << inputFile.rdbuf();

    inputFile.close();
    outputFile.close();

    return true;
}

bool NetFilter::pfRestore()
{
    bool retval = false;
    std::string fileName;

    if(workingDirectory == "")
        return false;

    strcpy(binpath, "");

    fileName = workingDirectory;
    fileName += "/";
    fileName += pfSaveFile;

    get_exec_path(pfctlBinary, binpath);

    if(strcmp(binpath, "") != 0 && access(fileName.c_str(), F_OK) == 0)
    {
        pfFlushAll();

        pfNeedsFullFlush = true;

        if(execute_process(NULL, NULL, binpath, "-f", fileName.c_str(), NULL) == 0)
        {
            unlink(fileName.c_str());

            retval = true;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfFlush()
{
    bool result = false;

    if(pfNeedsFullFlush == true)
        result = pfFlushAll();
    else
        result = pfFlushRules();

    return result;
}

bool NetFilter::pfFlushAll()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(pfctlBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        if(execute_process(NULL, NULL, cmdpath, "-F", "all", NULL) == 0)
        {
            retval = true;

            pfNeedsFullFlush = false;
        }
        else
            retval = false;
    }
    else
        retval = false;

    return retval;
}

bool NetFilter::pfFlushRules()
{
    bool retval = false;
    char cmdpath[64];

    strcpy(cmdpath, "");

    get_exec_path(pfctlBinary, cmdpath);

    if(strcmp(cmdpath, "") != 0)
    {
        if(execute_process(NULL, NULL, cmdpath, "-F", "nat", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "queue", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "rules", NULL) != 0)
            return false;

        if(execute_process(NULL, NULL, cmdpath, "-F", "Tables", NULL) != 0)
            return false;
    }
    else
        retval = false;

    return retval;
}

void NetFilter::pfSetup(const std::string &loopbackIface)
{
    pfResetRules();

    pfAddRule("set block-policy drop");
    pfAddRule("set ruleset-optimization basic");

    pfAddRule("set skip on { " + loopbackIface + " }");

    pfAddRule("scrub in all");

    pfAddRule("block in all");
    pfAddRule("block out all");

    // IPv4

    // Local networks

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 192.168.0.0/16");
    pfAddRule("pass in quick inet from 192.168.0.0/16 to 192.168.0.0/16");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 172.16.0.0/12");
    pfAddRule("pass in quick inet from 172.16.0.0/12 to 172.16.0.0/12");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 10.0.0.0/8");
    pfAddRule("pass in quick inet from 10.0.0.0/8 to 10.0.0.0/8");

    // Multicast

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 224.0.0.0/24");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 224.0.0.0/24");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 224.0.0.0/24");

    // Simple Service Discovery Protocol address

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 239.255.255.250/32");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 239.255.255.250/32");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 239.255.255.250/32");

    // Service Location Protocol version 2 address

    pfAddRule("pass out quick inet from 192.168.0.0/16 to 239.255.255.253/32");
    pfAddRule("pass out quick inet from 172.16.0.0/12 to 239.255.255.253/32");
    pfAddRule("pass out quick inet from 10.0.0.0/8 to 239.255.255.253/32");

    // ICMP

    // pfAddRule("pass quick proto icmp");

    // IPv6

    if(localNetwork->isIPv6Enabled() == true)
    {
        // Local networks

        pfAddRule("pass out quick inet6 from fe80::/10 to fe80::/10");
        pfAddRule("pass in quick inet6 from fe80::/10 to fe80::/10");
        pfAddRule("pass out quick inet6 from ff00::/8 to ff00::/8");
        pfAddRule("pass in quick inet6 from ff00::/8 to ff00::/8");

        // ICMP

        // pfAddRule("pass quick proto icmp6 all");
    }
}


bool NetFilter::pfAddIgnoredInterface(const std::string &interface)
{
    std::regex skipPattern("set skip on \\{.*\\}");
    std::string replace = "set skip on { " + loopbackInterface;
    std::string out;

    for(std::size_t i=0; i<ignoredInterface.size(); ++i)
    {
        replace += " ";
        replace += ignoredInterface[i];
    }

    replace += " }";

    pfRules = std::regex_replace(pfRules, skipPattern, replace);

    pfCommit();

    return true;
}

#if defined(__linux__) && !defined(__ANDROID__)

bool NetFilter::loadLinuxModule(const std::string &module_name, const std::string &module_params)
{
    return loadLinuxModule(module_name.c_str(), module_params.c_str());
}

bool NetFilter::loadLinuxModule(const char *module_name, const char *module_params)
{
    std::ostringstream os;
    int retval;
    bool result = false;

    retval = load_kernel_module(module_name, module_params);

    os.str("");

    switch(retval)
    {
        case MODULE_LOAD_SUCCESS:
        {
            os << "Successfully loaded kernel module " << module_name;

            result = true;
        }
        break;

        case MODULE_ALREADY_LOADED:
        {
            result = true;
        }
        break;

        case MODULE_NOT_FOUND:
        {
            os << "WARNING: Kernel module " << module_name << " not found. (" << retval << ")";

            result = false;
        }
        break;

        default:
        {
            os << "ERROR: Error while loading kernel module " << module_name << " (" << retval << ")";

            result = false;
        }
        break;
    }

    if(os.str() != "")
        global_log(os.str());

    return result;
}

#endif
