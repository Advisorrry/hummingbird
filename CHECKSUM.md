# Hummingbird for Linux and macOS

#### AirVPN's free and open source OpenVPN 3 client based on AirVPN's OpenVPN 3 library fork

### Version 1.2.0 - Release date 22 March 2022


## Note on Checksum Files

Hummingbird is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of hummingbird which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" hummingbird
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of Hummingbird to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the hummingbird client. Please note the files contained in the
distribution tarballs are created from the very source code available in the
master branch of the [official hummingbird's repository](https://gitlab.com/AirVPN/hummingbird).


### Checksum codes for Version 1.2.0

The checksum codes contained in files `hummingbird-<os>-<arch>-1.1.2.<archive>.sha512`
and `hummingbird.sha512` must correspond to the codes below in order to prove
they are genuinely created and distributed by AirVPN.


## Linux x86_64

`hummingbird-linux-x86_64-1.2.0.tar.gz`:
6b376b957720bc10aeedd24eeb57ea2808f8e993cb5b2631bc902a9318ac072220d6508bf85a170572dd90db2ca22e67b887e0b45dbb66de57a9b34d3fb4669e

`hummingbird`:
3a5db676c77c533673e14b5562fe0a3f0e31ec56052c8b79d4fb97c997186058696a27de7c453d62eaafdc29824885be2bcf3a98684c16d3658ed298c2cc4b60


## Linux ARM32

`hummingbird-linux-armv7l-1.2.0.tar.gz`:
92c974917e135331748f29431b3865742df2fca309c4e3ba80ac653e0c8460b609ba64e8963edb2194a684cd16fe5b84b52194347e361dbd91dfd026f2de905d

`hummingbird`:
077055e368f19fd73ef870cd2626aa72ffe2382e69c9551f64ebe266fa371a64d58a4d94144a1104da4213e97e8fe38f3362ae1185f9006ee24803b9b870eb4f


## Linux ARM32 Legacy

`hummingbird-linux-armv7l-legacy-1.2.0.tar.gz`:
c75f6c4a588ff83dca03956153f62ce9c4196fb1880a8a8db589e37bd023829010e828af49175aaaefec9c76b2b9a8a524c811bd3f01864aee0bf3f46df2d65e

`hummingbird`:
d501bc4d9cb57e432e4ff9a9b6ce177bdc25167a5642c879071c8477467c0361ff4cd1a626b34df401b45bc99f30208e2a51c7302f10e91a45b5e337024d2db3


## Linux ARM64

`hummingbird-linux-aarch64-1.2.0.tar.gz`:
905d5f68536a5f9f8e41af3478f7d8521af2fcd6cc827d544647defb9e438c8d579b45eca0c6db4906d22de9b8bf03ce438543aeea1d4b9c2d5c94677f2ed542

`hummingbird`:
3b172c2354b7fdf37a60c6020445c4f38ebb8186bed03d68631cf7f60c6f5342e44fe206d93873f539c3d5ba207fa97ac20eae93db4198c2d36f1c238b87417f


## macOS ARM64 plain

`hummingbird-macos-arm64-1.2.0.tar.gz`:
b089d753d0b975490ef8a918493e42a0e9ba339fcfed516b4b1cf392dbaab6d363fec9063d3add57949965247444fa2454b5e03b0cd11b4ee4e2dd98c2a629e0

`hummingbird`:
e5ff73ba0b166f0063a02f093d7a57621098dd4aacb027761dc3fe8a7aa3f4956ae4d5174709b627ddcb6f47477111a1b13b2a3143510ce25d4a2aa1dac0f24e


## macOS ARM64 notarized

`hummingbird-macos-arm64-notarized-1.2.0.zip`:
2fe918e9371e6080d9aa8b8bb6badd1358cad481195d13548783984b3dc92cf4fa90b00f0cd7e2dfed353bcfa7487b2a91e28707bbc2506db42af7d1ac0fa1e9

`hummingbird`:
c3a409a6b9d684c4c627938dc07643d6797ec0242107653b127cf757827f3f63a07ac9c27cfb710aaa89b514f6971c3754115fd5562d1eb412b4561e854cfd60


## macOS x86_64 plain

`hummingbird-macos-x86_64-1.2.0.tar.gz`:
88ed00ec33b1b87375b8008d97db3117123708ca185544d6e1930b1d5ae84e440249f3ab5962405d608a851f5bdc5d083dce8c15d4575889859adb81c77b312e

`hummingbird`:
6057d1203cc2668bad6cad64d64753c048725e80aae1323e93e923d8589831e975615b9ad6c2e84768def1f9ae4ca01aee1a1830153e4bae1ae7a18283cc5876


## macOS x86_64 notarized

`hummingbird-macos-x86_64-notarized-1.2.0.zip`:
6f870facdc560b0eb8eb5a3458b8c0d6276cb1f26c093c40b6c6042faac5f1140790c4ea5a311f25786c00478c536b1be3fe9a752587b284a96782b518233c3a

`hummingbird`:
61c6a3a6506cd74a4650d30e0477b158b613803ae3bc3b38b4cdc4fadf4c4a1316ec37d89a034d89eae6e717b906ba58c3c2a6f71ecd79cebe934d574020b049
