#!/bin/sh

#
# Build static binary for hummingbird and distribution tarball
#
# Version 1.1 - ProMIND
#

BASE_NAME=hummingbird
SSL_LIB_TYPE=OPENSSL

OS_NAME=`uname`
OS_ARCH_NAME=$(uname -m)

if [ "${OS_ARCH_NAME}" = "armv7l" ]; then
    lsb_release -d | grep -i buster > /dev/null 2>&1

    if [ $? -eq 0 ]; then
        OS_ARCH_NAME=${OS_ARCH_NAME}-legacy
    fi
fi

MIN_MACOS_VERSION=10.13

INC_DIR=..
OPENVPN3=${INC_DIR}/openvpn3-airvpn
ASIO=${INC_DIR}/asio

VERSION=$(grep -r "#define HUMMINGBIRD_VERSION" src/include/hummingbird.hpp | cut -f 2 -d \" | sed -e 's/ /-/g')

case $OS_NAME in
    Linux)
        BIN_FILE=${BASE_NAME}-linux-${OS_ARCH_NAME}
        SHA_CMD=sha512sum

        echo "Building static ${BASE_NAME} ${VERSION} for Linux ${OS_ARCH_NAME}"

        SOURCES="src/hummingbird.cpp \
                 src/localnetwork.cpp \
                 src/dnsmanager.cpp \
                 src/netfilter.cpp \
                 src/airvpntools.cpp \
                 src/optionparser.cpp \
                 src/execproc.c \
                 src/loadmod.c
                "

        case $OS_ARCH_NAME in
            i686)
                STATIC_LIB_DIR=/usr/lib/i386-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-i686
                break
                ;;

            x86_64)
                STATIC_LIB_DIR=/usr/lib/x86_64-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-x86_64
                break
                ;;

            armv7l | armv7l-legacy)
                STATIC_LIB_DIR=/usr/lib/arm-linux-gnueabihf
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-armv7l
                break
                ;;

            aarch64)
                STATIC_LIB_DIR=/usr/lib/aarch64-linux-gnu
                SSL_LIB_DIR=/usr/local/lib
                LOCAL_LIB_DIR=./lib-aarch64
                break
                ;;
        esac

        case $SSL_LIB_TYPE in
            OPENSSL)
                SSL_DEF=-DUSE_OPENSSL
                SSL_LIB_LINK=""
                break
                ;;

            MBEDTLS)
                SSL_DEF=-DUSE_MBEDTLS
                SSL_LIB_LINK="${SSL_LIB_DIR}/libmbedtls.a ${SSL_LIB_DIR}/libmbedx509.a ${SSL_LIB_DIR}/libmbedcrypto.a"
                break
                ;;

            *)
                echo "Unsupported SSL Library type ${SSL_LIB_TYPE}"
                exit 1
                ;;
        esac

        COMPILE="g++ -fwhole-program -Ofast -Wall -Wno-sign-compare -Wno-unused-parameter -std=c++14 -flto=4 -Wl,--no-as-needed -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread ${SSL_DEF} -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${ASIO}/asio/include -DHAVE_LZ4 -I${INC_DIR} -I${OPENVPN3} -I/usr/include/libxml2 ${SOURCES} ${LOCAL_LIB_DIR}/libcryptopp.a ${LOCAL_LIB_DIR}/libcurl.a ${SSL_LIB_LINK} -lssl -lcrypto ${STATIC_LIB_DIR}/liblz4.a ${STATIC_LIB_DIR}/libz.a ${STATIC_LIB_DIR}/liblzma.a ${STATIC_LIB_DIR}/libzstd.a -ldl -o ${BIN_FILE}"

        break
	    ;;

    Darwin)
        BIN_FILE=${BASE_NAME}-macos-${OS_ARCH_NAME}
        SHA_CMD="shasum -a 512"
        LIB_DIR=/usr/local/lib

        echo "Building static ${BASE_NAME} ${VERSION} for macOS ${OS_ARCH_NAME}"

        SOURCES="src/hummingbird.cpp \
                 src/localnetwork.cpp \
                 src/netfilter.cpp \
                 src/airvpntools.cpp \
                 src/optionparser.cpp \
                 src/base64.cpp \
                 src/execproc.c
                "

        case $OS_ARCH_NAME in
            x86_64)
                MIN_MACOS_VERSION=10.13
                break
                ;;

            aarch64)
                MIN_MACOS_VERSION=11.0
                break
                ;;
        esac

        case $SSL_LIB_TYPE in
            OPENSSL)
                SSL_DEF=-DUSE_OPENSSL
                SSL_LIB_LINK="${LIB_DIR}/libssl.a ${LIB_DIR}/libcrypto.a"
                break
                ;;

            MBEDTLS)
                SSL_DEF=-DUSE_MBEDTLS
                SSL_LIB_LINK="${LIB_DIR}/libmbedtls.a ${LIB_DIR}/libmbedx509.a ${LIB_DIR}/libmbedcrypto.a ${LIB_DIR}/libssl.a ${LIB_DIR}/libcrypto.a"
                break
                ;;

            *)
                echo "Unsupported SSL Library type ${SSL_LIB_TYPE}"
                exit 1
                ;;
        esac

        COMPILE="clang++ -Ofast -Wall -Wno-tautological-compare -Wno-unused-private-field -Wno-c++1y-extensions -mmacosx-version-min=${MIN_MACOS_VERSION} -framework CoreFoundation -framework SystemConfiguration -framework IOKit -framework ApplicationServices -Wno-sign-compare -Wno-unused-parameter -std=c++14 -Wunused-local-typedefs -Wunused-variable -Wno-shift-count-overflow -pthread -DBOOST_ASIO_DISABLE_KQUEUE ${SSL_DEF} -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -I${ASIO}/asio/include -DHAVE_LZ4 -I${OPENVPN3} ${SOURCES} -lz -lresolv ${SSL_LIB_LINK} ${LIB_DIR}/libcurl.a ${LIB_DIR}/libunistring.a ${LIB_DIR}/libiconv.a ${LIB_DIR}/liblz4.a ${LIB_DIR}/libxml2.a ${LIB_DIR}/libidn2.a ${LIB_DIR}/libcryptopp.a ${LIB_DIR}/liblzma.a -o ${BIN_FILE}"

	    break
	    ;;

    *)
	    echo "Unsupported system"
        exit 1
	    ;;
esac

OUT_DIR_NAME=${BIN_FILE}-${VERSION}
TAR_FILE_NAME=${OUT_DIR_NAME}.tar.gz
TAR_CHK_FILE_NAME=${TAR_FILE_NAME}.sha512

echo "SSL Library is ${SSL_LIB_TYPE}"

echo

echo "Compiling sources"

$COMPILE

if [ -d $OUT_DIR_NAME ]
then
    rm -r ${OUT_DIR_NAME}
fi

mkdir ${OUT_DIR_NAME}

strip ${BIN_FILE}

echo

echo "Done compiling ${BIN_FILE}"

echo

echo "Create tar file for distribution"

echo

cp ${BIN_FILE} ${OUT_DIR_NAME}/${BASE_NAME}
cp README-hb.md ${OUT_DIR_NAME}/README.md
cp LICENSE.md ${OUT_DIR_NAME}
cp Changelog-Hummingbird.txt ${OUT_DIR_NAME}/Changelog.txt

cd ${OUT_DIR_NAME}
$SHA_CMD ${BASE_NAME} > ${BASE_NAME}.sha512
cd ..

if [ -f ${TAR_FILE_NAME} ]
then
    rm ${TAR_FILE_NAME}
fi

tar czf ${TAR_FILE_NAME} ${OUT_DIR_NAME}

if [ -f ${TAR_CHK_FILE_NAME} ]
then
    rm ${TAR_CHK_FILE_NAME}
fi

$SHA_CMD ${TAR_FILE_NAME} > ${TAR_CHK_FILE_NAME}

rm -r ${OUT_DIR_NAME}

echo "tar file: ${TAR_FILE_NAME}"
echo "tar checksum file: ${TAR_CHK_FILE_NAME}"
echo
echo "Done."
